require "microframework/base"

class MyApp < Microframework::App
  get "/hello" do 
    "Modular hi!"
  end

  # test with a curl in terminal to pass a parameter: curl -d foo=bar http://localhost:3000/update
  # -d ensures it is sent as a post 
  post "/update" do 
    "updated"
  end
end